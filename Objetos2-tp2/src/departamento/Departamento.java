package departamento;
import java.util.ArrayList;

import persona.*;

public class Departamento {

	private String nombre;
	private ArrayList<Empleado> empleados = new ArrayList<Empleado>();
	
	
	//Constructor
	public Departamento(String nombre) {
		super();
		this.nombre = nombre;
		//this.empleados = empleados;
	}
	
	//Setters y Getters
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public ArrayList<Empleado> getEmpleados() {
		return empleados;
	}

	public void setEmpleados(ArrayList<Empleado> empleados) {
		this.empleados = empleados;
	}
	
	public void setEmpleado(Empleado empleado) {
		this.empleados.add(empleado);
	}
	
	//OK
	public float sueldoBrutoTotal() {
		
		float sueldoBrutoTotal = 0;
		for (Empleado empleado:this.getEmpleados()) {
			sueldoBrutoTotal = sueldoBrutoTotal + empleado.sueldoBruto();
		}
		return sueldoBrutoTotal;
	}
	
	//OK
	public float retencionesTotal() {
		
		float retencionTotal = 0;
		for (Empleado empleado:this.getEmpleados()) {
			retencionTotal = retencionTotal + empleado.retenciones();
		}
		return retencionTotal;
	}
	
	//OK
	public float sueldoNetoTotal() {
		
		return this.sueldoBrutoTotal() - this.retencionesTotal();
	}
	
	//OK
	public void liquidarSueldos() {
		for (Empleado empleado:this.getEmpleados()) {
			empleado.liquidarSueldo();
		}
	}
	
 
}
