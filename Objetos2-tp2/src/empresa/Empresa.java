package empresa;

import departamento.Departamento;

public class Empresa {
	
	//Variables de instancia
	private String nombre;
	private int cuit;
	private Departamento departamento;
	
	//Constructor
	public Empresa(String nombre, int cuit, Departamento departamento) {
		super();
		this.setNombre(getNombre());
		this.setCuit(cuit);
		this.setDepartamento(departamento);
	}
	
	//Setters y Getters
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getCuit() {
		return cuit;
	}

	public void setCuit(int cuit) {
		this.cuit = cuit;
	}

	public Departamento getDepartamento() {
		return departamento;
	}

	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}
	
	//Protocolo (métodos)
	//OK
	public float sueldoBrutoTotal() {
		//Retorna el calculo del sueldo bruto de todos los empleados del departamento.
		return this.getDepartamento().sueldoBrutoTotal();
	}
	
	//OK
	public float retencionesTotal() {
		//Retorna el calculo de las retenciones totales de todos los empleados del departamento.
		return this.getDepartamento().retencionesTotal();
	}
	
	//OK
	public float sueldoNetoTotal() {
		//Retorna el calculo del sueldo neto de todos los empleados del departamento.
		return this.getDepartamento().sueldoNetoTotal();
	}
	
	public void liquidarSueldos() {
		//Envía a liquidar los sueldos de todos los empleados del departamento.
		this.getDepartamento().liquidarSueldos();
	}
}
