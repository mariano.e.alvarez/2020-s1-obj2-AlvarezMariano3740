package empresa;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import persona.Empleado;

public class ReciboDeSueldo {
	
	private int legajo;
	private int idRecibo;
	private String nombre;
	private String direccion;
	private String fechaEmision;
	private String sueldoBruto;
	private String sueldoNeto;
	private ArrayList<String> conceptosSueldoBruto;
	private ArrayList<String> conceptosRetenciones;
	
	//Constructor	
	public ReciboDeSueldo(Empleado empleado) {
		super();
		this.setIdEmpleado(empleado.getId());
		this.setNombre(empleado.getNombre());
		this.setDireccion(empleado.getDireccion());
		this.fechaEmision = this.obtenerFechaActual();
		this.sueldoBruto = Float.toString(empleado.sueldoBruto());
		this.sueldoNeto = Float.toString(empleado.sueldoNeto());
		this.setConceptosSueldoBruto(empleado.conceptosSueldoBruto());
		this.setConceptosRetenciones(empleado.conceptosRetenciones());;
	}
	
	//Setters y Getters
	public String getSueldoBruto() {
		return sueldoBruto;
	}

	public void setSueldoBruto(String sueldoBruto) {
		this.sueldoBruto = sueldoBruto;
	}

	public String getSueldoNeto() {
		return sueldoNeto;
	}


	public void setSueldoNeto(String sueldoNeto) {
		this.sueldoNeto = sueldoNeto;
	}
	
	public int getIdEmpleado() {
		return legajo;
	}

	public void setIdEmpleado(int idEmpleado) {
		this.legajo = idEmpleado;
	}

	public int getIdRecibo() {
		return idRecibo;
	}

	public void setIdRecibo(int idRecibo) {
		this.idRecibo = idRecibo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getFechaEmision() {
		return fechaEmision;
	}

	public void setFechaEmision(String fechaEmision) {
		this.fechaEmision = fechaEmision;
	}

	public ArrayList<String> getConceptosSueldoBruto() {
		return conceptosSueldoBruto;
	}

	public void setConceptosSueldoBruto(ArrayList<String> conceptosSueldoBruto) {
		this.conceptosSueldoBruto = conceptosSueldoBruto;
	}
	
	public void setConceptoSueldoBruto(String conceptoSueldoBruto) {
		this.conceptosRetenciones.add(conceptoSueldoBruto);
	}

	public ArrayList<String> getConceptosRetenciones() {
		return conceptosRetenciones;
	}

	public void setConceptosRetenciones(ArrayList<String> conceptosRetenciones) {
		this.conceptosRetenciones = conceptosRetenciones;
	}
	
	public void setConceptoRetencion(String conceptoRetencion) {
		this.conceptosRetenciones.add(conceptoRetencion);
	}
	
	private String obtenerFechaActual() {
		
		Date fechaYHora = new Date();
		DateFormat formatoDeHoraYFecha = new SimpleDateFormat("EEE, d MMM yyyy");

		return formatoDeHoraYFecha.format(fechaYHora);
	}
	
	//Crear recibo de sueldo para un empleado
	public ReciboDeSueldo crearParaEmpleado(Empleado empleado) {
		ReciboDeSueldo recibo = new ReciboDeSueldo(empleado);
		return recibo;
	}
	
	public ArrayList<String> desgloceDeConceptos() {
		
		ArrayList<String> desgloceDeConceptos = new ArrayList<String>();
		
		desgloceDeConceptos.addAll(this.getConceptosSueldoBruto());
		desgloceDeConceptos.addAll(this.getConceptosRetenciones());
		
		return desgloceDeConceptos;
	}
}