package persona;

import java.util.ArrayList;

import empresa.ReciboDeSueldo;

public interface Empleado {
	
	public int getId();
	
	public String getNombre();
	
	public String getDireccion();
	
    public float sueldoNeto();
        
	public float sueldoBruto();
	
	public float retenciones();
	
	public ArrayList<String> conceptosRetenciones();

	public ArrayList<String> conceptosSueldoBruto();
	
	public ReciboDeSueldo generarRecibo();
	
	public void liquidarSueldo();
}