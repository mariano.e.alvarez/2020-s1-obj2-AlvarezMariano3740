package persona;

import java.util.ArrayList;

import medioDePago.MedioDePago;

public class EmpleadoContrato extends EmpleadoPlanta implements Empleado {
	
	private int numeroContrato;
	private MedioDePago medioDePago;
	private float gastoAdministrativosContable = 50; //Pasar el deptContable

	public EmpleadoContrato(String nombre, String direccion, int anioNacimiento, int mesNacimiento, int diaNaciomiento,
			Boolean casado, int cantHijos, float sueldoBasico, int anioIngreso, int mesIngreso, int diaIngreso, int id,
			int numeroContrato, MedioDePago medioDePago/*, float gastoAdministrativosContable*/) {
		super(nombre, direccion, anioNacimiento, mesNacimiento, diaNaciomiento, casado, cantHijos, sueldoBasico,
				anioIngreso, mesIngreso, diaIngreso, id);
		this.setNumeroContrato(numeroContrato);
		this.setMedioDePago(medioDePago);
	}

	@Override
	public float sueldoBruto() {
		return this.getSueldoBasico();
	}

	@Override
	public float retencionObraSocial() {
		return this.getGastoAdministrativosContable();
	}

	@Override
	public float retencionAporteJubilatorio() {
		return 0;
	}

	@Override
	public ArrayList<String> conceptosSueldoBruto() {
		//Calcula una lista de strings de retenciones necesaria para conformar el recibo de sueldo.
		//Cada elemento de la lista es un string que tiene cada concepto.
		//Metodo para el Recibo de Sueldo.
		
		ArrayList<String> conceptosSueldoBruto = new ArrayList<String>();
		
		//Armo el concepto
		String sueldoBasico = "Sueldo Basico: " + Float.toString(this.getSueldoBasico());
		
		//Agrego todos los conceptos a la lista de strings de retenciones.
		conceptosSueldoBruto.add(sueldoBasico);
		
		return conceptosSueldoBruto;
	
	}

	public int getNumeroContrato() {
		return numeroContrato;
	}

	public void setNumeroContrato(int numeroContrato) {
		this.numeroContrato = numeroContrato;
	}

	public MedioDePago getMedioDePago() {
		return medioDePago;
	}

	public void setMedioDePago(MedioDePago medioDePago) {
		this.medioDePago = medioDePago;
	}

	public float getGastoAdministrativosContable() {
		return gastoAdministrativosContable;
	}

	public void setGastoAdministrativosContable(float gastoAdministrativosContable) {
		this.gastoAdministrativosContable = gastoAdministrativosContable;
	}

}
