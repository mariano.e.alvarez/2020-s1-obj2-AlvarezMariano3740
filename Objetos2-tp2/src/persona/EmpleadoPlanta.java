package persona;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import empresa.ReciboDeSueldo;

public abstract class EmpleadoPlanta extends Persona implements Empleado{
	
	//LISTO
	//Setters privados
	
	//Variable de instancia
	private int idEmpleado;
	private float alicuotaJubilacion;
	private float alicuotaObraSocial = 10;
	private float sueldoBasico;
	private int anioIngreso;
	private int mesIngreso;
	private int diaIngreso;
	//Lista de recibos de sueldo
	private ArrayList<ReciboDeSueldo> recibos = new ArrayList<ReciboDeSueldo>();
	
	//Constructor
	public EmpleadoPlanta(String nombre, String direccion, int anioNacimiento, int mesNacimiento,
			int diaNaciomiento, Boolean casado, int cantHijos, float sueldoBasico,
			int anioIngreso, int mesIngreso, int diaIngreso, int id) {
		super(nombre, direccion, anioNacimiento, mesNacimiento, diaNaciomiento, casado, cantHijos);
		this.setSueldoBasico(sueldoBasico);
		this.setAnioIngreso(anioIngreso);
		this.setMesIngreso(mesIngreso);
		this.setDiaIngreso(diaIngreso);
		this.setId(id);
		//EmpleadoPlanta.setDeptoContable(deptoContable);
		//EmpleadoPlanta.deptoContable.setEmpleado(this);
		//this.asignarEmpleadoADeptoContable();

	}

	//Getters y Setters
	public ArrayList<ReciboDeSueldo> getRecibos() {
		return recibos;
	}
	
	private void setRecibo(ReciboDeSueldo recibo) {
		this.recibos.add(recibo);
	}

	public int getId() {
		return idEmpleado;
	}
	
	private void setId(int id) {
		this.idEmpleado = id;
	}

	public float getAlicuotaObraSocial() {
		return alicuotaObraSocial;
	}

//	private void setAlicuotaObraSocial(float alicuotaObraSocial) {
//		this.alicuotaObraSocial = alicuotaObraSocial;
//	}

	public float getAlicuotaJubilacion() {
		return alicuotaJubilacion;
	}
	
	public void setAlicuotaJubilacion(float alicuotaJubilacion) {
		this.alicuotaJubilacion = alicuotaJubilacion;
	}
	
	public float getSueldoBasico() {
		return sueldoBasico;
	}

	private void setSueldoBasico(float sueldoBasico) {
		this.sueldoBasico = sueldoBasico;
	}
	
	public int getAnioIngreso() {
		return anioIngreso;
	}
	
	private void setAnioIngreso(int anioIngreso) {
		this.anioIngreso = anioIngreso;
	}
	
	public int getMesIngreso() {
		return mesIngreso;
	}
	
	private void setMesIngreso(int mesIngreso) {
		this.mesIngreso = mesIngreso;
	}
	
	public int getDiaIngreso() {
		return diaIngreso;
	}
	
	private void setDiaIngreso(int diaIngreso) {
		this.diaIngreso = diaIngreso;
	}
	
	//Protocolo (metodos)
    public int antiguedad() {
    	/*
    	 * Devuelve la antiguedad en anios de un empleado
    	Antiguedad (en anios) = fechaActual - fechaIngreso
    	*/
    	Calendar fechaIngreso = new GregorianCalendar(this.getAnioIngreso(),this.getMesIngreso(),this.getDiaIngreso());
        Calendar fechaActual = Calendar.getInstance();
 
        // Cálculo de las diferencias.
        int anios = fechaActual.get(Calendar.YEAR) - fechaIngreso.get(Calendar.YEAR);
        int meses = fechaActual.get(Calendar.MONTH) - fechaIngreso.get(Calendar.MONTH);
        int dias = fechaActual.get(Calendar.DAY_OF_MONTH) - fechaIngreso.get(Calendar.DAY_OF_MONTH);
 
        // Hay que comprobar si el día de su cumpleaños es posterior
        // a la fecha actual, para restar 1 a la diferencia de años,
        // pues aún no ha sido su cumpleaños.
 
        if(meses < 0 // Aún no es el mes de su cumpleaños
           || (meses==0 && dias < 0)) { // o es el mes pero no ha llegado el día.
            anios--;
        }
        return anios;
    }
	
    public float sueldoNeto() {
    	//Devuelve el sueldo neto
    	return this.sueldoBruto() - this.retenciones();
    }
    
    public float porcentajeObraSocial() {
    	//Devuelve el porcentaje de descuento para la obra social
    	return this.getAlicuotaObraSocial()/100 * this.sueldoBruto();
    }
    
	public ReciboDeSueldo generarRecibo() {
		//Generar recibo de sueldo para este empleado
		ReciboDeSueldo recibo = new ReciboDeSueldo(this);
		return recibo;
	}
    
	public void liquidarSueldo() {
		//Liquidar sueldo --> (sueldoNeto = sueldoBruto - retenciones), luego generar reciboDeSueldo y luego almacenar.
		ReciboDeSueldo recibo = this.generarRecibo();
		this.setRecibo(recibo);
	}
	
	public float retenciones() {
		//Devuelve las retenciones totales del empleado.
		return this.retencionObraSocial() + this.retencionAporteJubilatorio();
	}
	
	public ArrayList<String> conceptosRetenciones() {
		
		//Devuelve una lista de strings con el concepto de retenciones.
		//Cada elemento de la lista es un string que tiene cada concepto.
		//Metodo para el Recibo de Sueldo.
		
		ArrayList<String> conceptosRetenciones = new ArrayList<String>();
		
		String retencionObraSocial = "Aporte ObraSocial: -" + Float.toString(this.retencionObraSocial());
		String retencionAporteJubilatorio = "Aporte Jubilatorio: -" + Float.toString(this.retencionAporteJubilatorio());
		
		conceptosRetenciones.add(retencionObraSocial);
		conceptosRetenciones.add(retencionAporteJubilatorio);
		
		return conceptosRetenciones;
	}
    
    //Metodos abstractos
	
	abstract public float sueldoBruto();						//Devuelve el sueldoBruto segun el tipo de empleado.
	
	public abstract float retencionObraSocial();				//Devuelve las retencion de la obra social segun el tipo de empleado.
	
	public abstract float retencionAporteJubilatorio();			//Devuelve las retencion del aporte jubilatorio segun el tipo de empleado.

	public abstract ArrayList<String> conceptosSueldoBruto();	//Lista de conceptos de conforma el sueldo bruto (para el desglose).
	
}