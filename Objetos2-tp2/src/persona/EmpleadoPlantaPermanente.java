package persona;

import java.util.ArrayList;

public class EmpleadoPlantaPermanente extends EmpleadoPlanta implements Empleado{
	
	//LISTO
	
	//private float alicuotaJubilacion;
	private float montoXHijo = 150;
	private float montoXAnioDeAntiguedad = 50;
	private float montoXConyuge = 100;
	
	public EmpleadoPlantaPermanente(String nombre, String direccion, int anioNacimiento,
			int mesNacimiento, int diaNaciomiento, Boolean casado, int cantHijos,
			float sueldoBasico, int anioIngreso, int mesIngreso, int diaIngreso, int id) {
		super(nombre, direccion, anioNacimiento, mesNacimiento, diaNaciomiento, casado, cantHijos,
				 sueldoBasico, anioIngreso, mesIngreso, diaIngreso, id);
		this.setAlicuotaJubilacion(15); // ---> Preguntar
	}
	
	//Setters y Getters
	public float getMontoXHijo() {
		return montoXHijo;
	}
	
	public void setMontoXHijo(float montoXHijo) {
		this.montoXHijo = montoXHijo;
	}
	
	public float getMontoXAnioDeAntiguedad() {
		return montoXAnioDeAntiguedad;
	}
	
	public void setMontoXAnioDeAntiguedad(float montoXAnioDeAntiguedad) {
		this.montoXAnioDeAntiguedad = montoXAnioDeAntiguedad;
	}
	
	public float getMontoXConyuge() {
		return montoXConyuge;
	}
	
	public void setMontoXConyuge(float montoXConyuge) {
		this.montoXConyuge = montoXConyuge;
	}
	
	
	/*	El sueldo bruto del empleado de planta permanente se compone de:
	Sueldo Básico
	Salario Familiar, que se compone de:
		Asignación por hijo: $150 por cada hijo.
		Asignación por cónyuge: $100 si tiene cónyuge
	Antigüedad: $50 por cada año de antigüedad.*/
	
	//Protocolo
	
	@Override
	public float sueldoBruto() {
		//Devuelve el sueldoBruto = sueldoBasico + salarioFamiliar + (asignacion x conyuge) + (asignacion x antiguedad).
		return this.getSueldoBasico() + this.asignacionXHijos() + this.asignacionXConjuge() + this.asignacionXAntiguedad();
	}
	
	private float asignacionXConjuge() {
		//Devuelve laasignacion por conyuge.
		return this.getCasado() ? this.getMontoXConyuge() : 0;
	}
	
	private float asignacionXHijos() {
		//Devuelve la asignacion por hijos.
		return this.getMontoXHijo() * this.getCantHijos();
	}
	
	private float asignacionXAntiguedad() {
		//Devuelve la asignacion por antiguedad.
		return this.antiguedad() * this.getMontoXAnioDeAntiguedad();
	}

	public float retencionObraSocial() {
		//Devuelve las retencion de la obra social segun el tipo de empleado.
		return this.porcentajeObraSocial() + (this.getMontoXHijo() * this.getCantHijos());
	}
	
	public float retencionAporteJubilatorio() {
		//Devuelve las retencion del aporte jubilatorio segun el tipo de empleado.
		return (this.getAlicuotaJubilacion()/100) * this.sueldoBruto();
	}
		
	@Override
	public ArrayList<String> conceptosSueldoBruto() {
		
		//Devuelve una lista de strings de retenciones necesaria para conformar el recibo de sueldo.
		//Cada elemento de la lista es un string que tiene cada concepto.
		//Metodo para el Recibo de Sueldo.
		
		ArrayList<String> conceptosSueldoBruto = new ArrayList<String>();
		
		//Armo el concepto
		String sueldoBasico = "Sueldo Basico: " + Float.toString(this.getSueldoBasico());
		String asignacionXHijos = "Asignacion por Hijos: " + Float.toString(this.asignacionXHijos());
		String asignacionXConyuge = "Asignacion por Conyuge: " + Float.toString(this.asignacionXConjuge());
		String asignacionXAntiguedad = "Asignacion por Antiguedad: " + Float.toString(this.asignacionXAntiguedad());
		
		//Agrego todos los conceptos a la lista de strings de retenciones.
		conceptosSueldoBruto.add(sueldoBasico);
		conceptosSueldoBruto.add(asignacionXHijos);
		conceptosSueldoBruto.add(asignacionXConyuge);
		conceptosSueldoBruto.add(asignacionXAntiguedad);
		
		return conceptosSueldoBruto;
	}
}
