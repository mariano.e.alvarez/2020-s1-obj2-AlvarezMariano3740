package persona;

import java.util.ArrayList;

public class EmpleadoPlantaTemporaria extends EmpleadoPlanta implements Empleado{
	
	//LISTO
	
	//Variables de instancia
	private float cantHorasExtras = 0;
	private static float montoX50Anios = 25;
	private static float montoXHoraExtra = 40;

	public EmpleadoPlantaTemporaria(String nombre, String direccion, int anioNacimiento,
			int mesNacimiento, int diaNaciomiento, Boolean casado, int cantHijos, float sueldoBasico, int anioIngreso,
			int mesIngreso, int diaIngreso, int id) {
		super(nombre, direccion, anioNacimiento, mesNacimiento, diaNaciomiento, casado, cantHijos,
				sueldoBasico, anioIngreso, mesIngreso, diaIngreso, id);
		this.setAlicuotaJubilacion(10); // ---> Preguntar
	}
	
	//Settets y Getters
	public float getMontoXHoraExtra() {
		return montoXHoraExtra;
	}
	
	public void setMontoXHoraExtra(float montoXHoraExtra) {
		EmpleadoPlantaTemporaria.montoXHoraExtra = montoXHoraExtra;
	}
		
	public float getCantHorasExtras() {
		return cantHorasExtras;
	}

	public void setCantHorasExtras(float cantHorasExtras) {
		this.cantHorasExtras = cantHorasExtras;
	}
	
	public float getMontoX50Anios() {
		return montoX50Anios;
	}
	
	public void setMontoX50Anios(float montoX50Anios) {
		EmpleadoPlantaTemporaria.montoX50Anios = montoX50Anios;
	}

	//Protocolo

	@Override
	public float sueldoBruto() {
		//Calcula el sueldoBruto = sueldoBasico + asignaciones x horas extras.
		return this.getSueldoBasico() +  this.asignacionXHorasHextras();
	}
	
	private float asignacionXHorasHextras() {
		//Calcula el monto de la asignacion por horas extras trabajadas.
		return this.getMontoXHoraExtra() * this.getCantHorasExtras();
	}
	
	public float retencionObraSocial() {
		//Calcula las retencion de la obra social segun el tipo de empleado.
		return this.porcentajeObraSocial() + (this.getMontoX50Anios() * this.superaLaEdadDe(50));
	}
	
	public float retencionAporteJubilatorio() {
		//Calcula las retencion del aporte jubilatorio segun el tipo de empleado.
		return ((this.getAlicuotaJubilacion()/100) * this.sueldoBruto()) + 5 * this.getCantHorasExtras();
	}

	private int superaLaEdadDe(int edad) {
		//Calcula verdadero si se supera la edad pasada como parametro.
		return this.edad() > edad ? 1 : 0;
	}
	
	@Override
	public ArrayList<String> conceptosSueldoBruto() {
		
		//Calcula una lista de strings de retenciones necesaria para conformar el recibo de sueldo.
		//Cada elemento de la lista es un string que tiene cada concepto.
		//Metodo para el Recibo de Sueldo.
		
		ArrayList<String> conceptosSueldoBruto = new ArrayList<String>();
		
		//Armo el concepto
		String sueldoBasico = "Sueldo Basico: " + Float.toString(this.getSueldoBasico());
		String asignacionXHorasExtras = "Asignacion por Horas Extras: " + Float.toString(this.asignacionXHorasHextras());
		
		//Agrego todos los conceptos a la lista de strings de retenciones.
		conceptosSueldoBruto.add(sueldoBasico);
		conceptosSueldoBruto.add(asignacionXHorasExtras);
		
		return conceptosSueldoBruto;
	}
}
