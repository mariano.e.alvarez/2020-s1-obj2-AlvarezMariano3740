package persona;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class Persona{
	
	//LISTO
	//Setters privados

	//Variables de instancia
	private String nombre;
	private String direccion;
	private Calendar fNac;
	private int anioNacimiento;
	private int mesNacimiento;
	private int diaNaciomiento;
	private Boolean	casado;
	private int cantHijos;	
	
	//Constructor
	public Persona(String nombre, String direccion, int anioNacimiento, int mesNacimiento,
			int diaNaciomiento, Boolean casado, int cantHijos) {
		super();
		
		this.setNombre(nombre);
		this.setDireccion(direccion);
		this.setAnioNacimiento(anioNacimiento);
		this.setMesNacimiento(mesNacimiento);
		this.setDiaNaciomiento(diaNaciomiento);
		this.setCasado(casado);
		this.setCantHijos(cantHijos);
	}
	
	//Setters y Getters
	public String getNombre() {
		return nombre;
	}
	
	private void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getDireccion() {
		return direccion;
	}
	
	private void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public Calendar getfNac() {
		return fNac;
	}
	
//	private void setfNac(Calendar fNac) {
//		this.fNac = fNac;
//	}
	
	public Boolean getCasado() {
		return casado;
	}

	private void setCasado(Boolean casado) {
		this.casado = casado;
	}

	public int getCantHijos() {
		return cantHijos;
	}
	
	private void setCantHijos(int cantHijos) {
		this.cantHijos = cantHijos;
	}

	public int getAnioNacimiento() {
		return anioNacimiento;
	}

	private void setAnioNacimiento(int anioNacimiento) {
		this.anioNacimiento = anioNacimiento;
	}

	public int getMesNacimiento() {
		return mesNacimiento;
	}

	private void setMesNacimiento(int mesNacimiento) {
		this.mesNacimiento = mesNacimiento;
	}

	public int getDiaNaciomiento() {
		return diaNaciomiento;
	}

	private void setDiaNaciomiento(int diaNaciomiento) {
		this.diaNaciomiento = diaNaciomiento;
	}
	
	//Protocolo (Metodos)
    public int edad() {
    	/*
    	 * Devuelve la edad de la persona
    	*/
    	Calendar fechaNac = new GregorianCalendar(this.getAnioNacimiento(),this.getMesNacimiento(),this.getDiaNaciomiento());
        Calendar fechaActual = Calendar.getInstance();
 
        // Cálculo de las diferencias.
        int anios = fechaActual.get(Calendar.YEAR) - fechaNac.get(Calendar.YEAR);
        int meses = fechaActual.get(Calendar.MONTH) - fechaNac.get(Calendar.MONTH);
        int dias = fechaActual.get(Calendar.DAY_OF_MONTH) - fechaNac.get(Calendar.DAY_OF_MONTH);
 
        // Hay que comprobar si el día de su cumpleaños es posterior
        // a la fecha actual, para restar 1 a la diferencia de años,
        // pues aún no ha sido su cumpleaños.
 
        if(meses < 0 // Aún no es el mes de su cumpleaños
           || (meses==0 && dias < 0)) { // o es el mes pero no ha llegado el día.
            anios--;
        }
        return anios;
    }
}
